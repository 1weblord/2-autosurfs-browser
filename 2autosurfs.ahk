#Persistent
#include COM.ahk
COM_AtlAxWinInit()
ComObjError(false)
Enabled := ComObjError(Enable)
;SetTimer, WatchForMenu, 50
;return  ; End of auto-execute section.

SetTimer, CloseUnwantedWindow, 250
Return

;SetTimer, KillTheWindow, 50
;Return


OnExit , GUIClose
Gui, +LastFound +Resize
pwb1 := COM_AtlAxGetControl(COM_AtlAxCreateContainer(WinExist(),0,0,600,300, "Shell.Explorer") )
pwb2 := COM_AtlAxGetControl(COM_AtlAxCreateContainer(WinExist(),0,300,600,300, "Shell.Explorer") )
gui,show, w600 h600 ,Autosurfpro and AutosurfJunction by: William R. Nabaza 
url1:="http://www.autosurfpro.com/cgi-bin/run.cgi?userid=weblord"
COM_Invoke(pwb1, "Navigate", url1)
url2:="http://www.autosurfjunction.com/cgi-bin/run.cgi?userid=weblord"
COM_Invoke(pwb2, "Navigate", url2)
return

;KillTheWindow:
;If WinActive("Sponsored session")
;  Send, {Enter}
;return  

CloseUnwantedWindow:
IfWinExist, Script Error, An error has occurred in the script on this page
	WinClose, Script Error, An error has occurred in the script on this page
Return

;WatchForMenu:
;DetectHiddenWindows, on  ; Might allow detection of menu sooner.
;IfWinExist, ahk_class MFCPopupWindow
;   WinHide, ahk_class MFCPopupWindow  ; Uses the window found by the above line.
;return

GuiClose:
COM_Release(pwb1) ; Always release COM objects
COM_Release(pwb2) ; Always release COM objects
COM_Term()
ExitApp
return